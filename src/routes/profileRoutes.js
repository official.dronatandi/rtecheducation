// routes/profileRoutes.js
const express = require('express');
const router = express.Router();
const { verifyToken } = require('../middlewares/authMiddleware');
const profileController = require('../controllers/profileController');

// Create or update user profile
router.post('/profile', verifyToken, profileController.createOrUpdateProfile);
router.get('/getMyprofile', verifyToken, profileController.getMyProfile);
router.get('/getAllprofile', verifyToken, profileController.getAllProfile);


module.exports = router;
