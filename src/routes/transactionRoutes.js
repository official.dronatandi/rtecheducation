const express = require('express');
const router = express.Router();
const { verifyToken } = require('../middlewares/authMiddleware');
const transactionContoller = require('../controllers/transactionsController');


// Create transaction
router.post('/transactions', verifyToken, transactionContoller.createTransaction);
router.get('/getAllTransaction', verifyToken, transactionContoller.getAllTransaction);


module.exports = router;