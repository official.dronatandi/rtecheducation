const express = require('express');
const router = express.Router();
const { verifyToken } = require('../middlewares/authMiddleware');
const sessionContoller = require('../controllers/sessionController');


// Create transaction
router.post('/createSession', verifyToken, sessionContoller.createSession);
router.get('/getSession', verifyToken, sessionContoller.getSession);


module.exports = router;