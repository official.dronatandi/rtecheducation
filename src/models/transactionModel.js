const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');
const User = require('./userModel');
const Session = require('./sessionModel');



const Transaction = sequelize.define('transaction', {
 
  transaction_name: {
    type: DataTypes.STRING,
  },
  amount: {
    type: DataTypes.STRING,
  },

  category: {
    type: DataTypes.STRING,
  },
  payment_mode: {
    type: DataTypes.STRING,
  },

  transaction_date: {
    type: DataTypes.STRING,
  },

  imageUrl: {
    type: DataTypes.STRING,
  },
  

}
);
Transaction.belongsTo(User); 
Transaction.belongsTo(Session);


module.exports = Transaction;
