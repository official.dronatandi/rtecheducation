const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');
const User = require('./userModel');


const Profile = sequelize.define('Profile', {
  email: {
    type: DataTypes.STRING,
    allowNull: false,
  },

  firstname: {
    type: DataTypes.STRING,
  },
  lastname: {
    type: DataTypes.STRING,
  },

  imageUrl: {
    type: DataTypes.STRING,
  },

  bio: {
    type: DataTypes.TEXT,
  },
}
);

User.hasOne(Profile, { foreignKey: 'userId' });
Profile.belongsTo(User, { foreignKey: 'userId' });

module.exports = Profile;
