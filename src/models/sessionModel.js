const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');

const Session = sequelize.define('Session', {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false,
  },

  start_date: {
    type: DataTypes.STRING,
    allowNull: false,
  },

  end_date: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  
});


module.exports = Session;
