const User = require('../models/userModel');
const Transaction = require('../models/transactionModel');


// Controller function for handling transaction creation
const createTransaction = async (req, res) => {
  try {
    const { userId } = req.user;
    const { transaction_name, amount, category, payment_mode, transaction_date, imageUrl } = req.body;

    // Check if the user exists
    let user = await User.findByPk(userId);

    if (!user) {
      return res.status(404).json({ error: 'User not found.' });
    }

    // Create transaction
    const transaction = await Transaction.create({
      transaction_name,
      amount,
      category,
      payment_mode,
      transaction_date,
      imageUrl,
      UserId: user.id,
    });

    res.status(201).json({
      message: 'Transaction created successfully.',
      transaction: transaction.toJSON(),
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

const getAllTransaction = async (req, res) => {
  try {
    const { userId } = req.user;

    const transactionList = await Transaction.findAll();

    if (!transactionList) {
      return res.status(404).json({ error: 'Transaction list not found.' });
    }

    const transactionResponse = transactionList;


    res.status(200).json({ transaction: transactionResponse });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

const createTransaction1 = async (req, res) => {
  try {
    // Extract necessary data from the request object
    const { userId } = req.user;
    const { transaction_name, amount, category, payment_mode, transaction_date, imageUrl } = req.body;

    // Retrieve the session from the request
    const session = req.session;

    // Check if the session is defined
    if (!session || !session.id) {
      return res.status(400).json({ error: 'Session not found.' });
    }

    // Check if the user exists
    const user = await User.findByPk(userId);
    if (!user) {
      return res.status(404).json({ error: 'User not found.' });
    }

    // Create transaction associated with the current session
    const transaction = await Transaction.create({
      transaction_name,
      amount,
      category,
      payment_mode,
      transaction_date,
      imageUrl,
      UserId: user.id,
      SessionId: session.id,
    });

    // Respond with a success message and created transaction details
    res.status(201).json({
      message: 'Transaction created successfully.',
      transaction: transaction.toJSON(),
    });
  } catch (error) {
    // Handle any errors
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};


module.exports = { createTransaction , getAllTransaction};
