const Session = require('../models/sessionModel');


const createSession = async (req, res) => {
    try {
      const { start_date, end_date } = req.body;
  
      // Check if the session exists
      let session = await Session.findOne({ where: { start_date, end_date } });
  
      // If the session doesn't exist, create a new one
      if (!session) {
        session = await Session.create({
          start_date,
          end_date,
        });
        
        // Convert session to JSON
        const sessionResponse = session.toJSON();
  
        res.status(200).json({ message: 'Session created successfully.', session: sessionResponse });
      } else {
        // If the session already exists, respond with a message indicating it
        res.status(201).json({ message: 'Session already exists.' });
      }
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  };

  const getSession = async (req, res) => {
    try {
      // Retrieve all sessions from the database
      const sessions = await Session.findAll();
  
      // Respond with the retrieved sessions
      res.status(200).json({ sessions });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  };


module.exports = {
  createSession, getSession,

};