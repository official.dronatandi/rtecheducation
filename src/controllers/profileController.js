const User = require('../models/userModel');
const profile = require('../models/profileModel');


const createOrUpdateProfile = async (req, res) => {
  try {
    const { userId,email } = req.user;
    const { firstname, lastname, bio, imageUrl } = req.body;

    // Check if the user exists
    const user = await User.findByPk(userId);

    if (!user) {
      return res.status(404).json({ error: 'User not found.' });
    }

    // Check if the user profile exists
    let userProfile = await profile.findOne({ where: { userId } });

    // If the user profile doesn't exist, create a new one
    if (!userProfile) {
      userProfile = await profile.create({
        userId,
        email,
        firstname,
        lastname,
        bio,
        imageUrl,
      });
    } else {
      // Update user profile fields
      userProfile.firstname = firstname !== undefined ? firstname : userProfile.firstname;
      userProfile.lastname = lastname !== undefined ? lastname : userProfile.lastname;
      userProfile.bio = bio !== undefined ? bio : userProfile.bio;
      userProfile.imageUrl = imageUrl !== undefined ? imageUrl : userProfile.imageUrl;

      // Save the changes
      await userProfile.save();
    }

    const userProfileResponse = userProfile.toJSON();

    // Remove sensitive information
    if (userProfileResponse.password) {
      delete userProfileResponse.password;
    }

    res.status(200).json({ message: 'Profile created/updated successfully.', userProfile: userProfileResponse });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};


const getMyProfile = async (req, res) => {
  try {
    const { userId } = req.user;

    // Check if the user profile exists
    const userProfile = await profile.findOne({ where: { userId } });

    if (!userProfile) {
      return res.status(404).json({ error: 'User profile not found.' });
    }

    // Ensure that the logged-in user matches the profile owner
    if (userProfile.userId !== userId) {
      return res.status(403).json({ error: 'Access denied. Unauthorized user.' });
    }

    const userProfileResponse = userProfile.toJSON();

    // Remove sensitive information
    if (userProfileResponse.password) {
      delete userProfileResponse.password;
    }

    res.status(200).json({ userProfile: userProfileResponse });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};


const getAllProfile = async (req, res) => {
  try {
    const { userId } = req.user;

    // Check if the user profile exists
    const userProfile = await profile.findAll();

    if (!userProfile) {
      return res.status(404).json({ error: 'User profile not found.' });
    }

    const userProfileResponse = userProfile;

    // Remove sensitive information
    if (userProfileResponse.password) {
      delete userProfileResponse.password;
    }

    res.status(200).json({ userProfile: userProfileResponse });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};




module.exports = {createOrUpdateProfile, getMyProfile, getAllProfile };
