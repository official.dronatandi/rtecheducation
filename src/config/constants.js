exports.constants = {
  VALIDATION_ERROR: 400,
  UNAUTHORIZED: 401,
  FORBIDDEN: 403,
  NOT_FOUND: 404,
  SERVER_ERROR: 500,

  //database constant
  HOST: "localhost",
  USER : "root",
  PASSWORD :"",
  DATABASE :"happy_home",
  DATABASE_TEST :"rtech_education_test",
  DATABASE_PROD :"rtech_education",



  appConstants:{
    ACCESS_DENIDE : "Access denied. No token provided.",
    INVALID_TOKEN :"Invalid token."

  }

};
