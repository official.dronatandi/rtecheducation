const { constants } = require("./constants");

module.exports = {
    db: {
      host: constants.HOST,
      user: constants.USER,
      password: constants.PASSWORD,
      //change database according to your requirement
      database: constants.DATABASE
    },
    jwtSecret: 'your-secret-key'
  };
  
